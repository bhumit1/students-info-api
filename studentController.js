const student = require("../models/User");

async function getStudents () {
    return student
}



async function saveStudents  (userInput){

    const User = {
        id: student.length + 1,
        name: userInput.name,
        email: userInput.email,
        gender: userInput.gender,
        phone: userInput.phone,
        password: userInput.password,
        city: userInput.city,
        state: userInput.state
    }
    student.push(User)
    return User
}

async function updateStudent (userId , userInput){
    let index = student.findIndex((participants) => {
        return (participants.id == Number.parseInt(userId))
    })
    console.log(index, "index");
    if (index >= 0) {
        let std = student[index]
        std.name = userInput.name
        std.email = userInput.email
        std.gender =userInput.gender
        std.phone = userInput.phone
        std.password = userInput.password
        std.city = userInput.city
        std.state = userInput.state

        return std
    }
}

async function deleteStudent (userId) {
console.log(userId);
console.log(JSON.stringify(student));
    let index = student.findIndex((participants) => {
        return (participants.id == Number.parseInt(userId))
    })
    console.log(index);
    if (index >= 0) {
        let std = student[index]
        student.splice(index, 1)
       console.log(std);
        return std
    }
}

module.exports = {
    getStudents,
    saveStudents,
    updateStudent,
    deleteStudent
}
