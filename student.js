const express = require("express");
const router = new express.Router();
const student = require("../models/User");
const studentUser = require("../controllers/studentController");

router.get('/', (req, res) => {
    res.json({ message: "Api is working" })
})

router.get('/getStudents', async(req, res) => {
    const students =  await studentUser.getStudents ();
    if (students){
        res.json(students)
    }
    else
    res.status(400)
})

router.post('/saveStudents', async (req, res) => {
    if (!req.body.email) {
        res.status(400)
        return res.json({ error: "email is required..." })
    }
    if (!req.body.name) {
        res.status(400)
        return res.json({error: "name is required..."})
    }
    const user = await studentUser.saveStudents(req.body)
   if (user) {
    res.json(user)
   }
     else {
         res.status(400)
     }

})
router.put('/updateStudentInfo/:id', async (req, res) => {
  console.log(req.params.id , req.body);
    const updateUser = await studentUser.updateStudent ( req.params.id,req.body);
    if (updateUser) {
        res.json(updateUser)
    }
    else {
        res.status(400)
    }
})

router.delete("/deleteStudents/:id", async (req, res) => {
     const deleteUser = await studentUser.deleteStudent (req.params.id);
     if (deleteUser){
         res.json(deleteUser)
     }
   else {
        res.status(400)
    }
})

module.exports = router;
